export function getRandomString(length: number): string {
  let str = '';
  const randomChar = () => {
    const n = Math.floor(Math.random() * 62);
    if (n < 10) return n; //1-10
    if (n < 36) return String.fromCharCode(n + 55); //A-Z
    return String.fromCharCode(n + 61); //a-z
  };

  while (str.length < length) {
    str += randomChar();
  }

  return str;
}

export function getRandomNumber(): number {
  return Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
}

export function getRandomBoolean(): boolean {
  return Math.random() < 0.5;
}

const STRING_TYPE = 1;
const NUMBER_TYPE = 2;
const BOOLEAN_TYPE = 3;

export function getRandomValue(): string | number | boolean {
  const type = Math.floor(Math.random() * 3 + 1);
  switch (type) {
    default:
    case STRING_TYPE:
      return getRandomString(Math.floor(Math.random() * 32));
    case NUMBER_TYPE:
      return getRandomNumber();
    case BOOLEAN_TYPE:
      return getRandomBoolean();
  }
}
