from dataclasses import dataclass
from email.message import Message
from string import Template
from typing import Dict, List

from aiosmtplib import SMTP, SMTPRecipientsRefused, SMTPResponseException

from .datastructures import Field


@dataclass(frozen=True)
class SMTPConfig:
    host: str
    port: int
    sender: str
    recipients: List[str]
    fields: List[Field]
    text_message_template: Template
    html_message_template: Template


SMTP_CONFIGS: Dict[str, SMTPConfig] = {}
SMTP_CLIENTS: Dict[str, SMTP] = {}


def smtp_client(host: str, port: int) -> SMTP:
    address = f"{host}:{port}"
    if address not in SMTP_CLIENTS:
        SMTP_CLIENTS[address] = SMTP(hostname=host, port=port, timeout=1)

    return SMTP_CLIENTS[address]


async def send_smtp_message(client: SMTP, message: Message) -> bool:
    try:
        # Sending message in SMTP client context.
        # This way it connects and closes automatically.
        async with client:
            await client.send_message(message)
    except (SMTPRecipientsRefused, SMTPResponseException):
        return False

    return True
