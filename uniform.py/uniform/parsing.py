from typing import Dict, List, Tuple, Union

from starlette.requests import Request

from .datastructures import Field
from .limits import LIMITS


async def parse_form(
    request: Request, fields: List[Field]
) -> Tuple[Dict[str, Union[float, str, None]], Dict[str, List[str]]]:
    form = await request.form()

    parsed_fields: Dict[str, Union[float, str, None]] = {}
    errors: Dict[str, List[str]] = {}

    for field in fields:
        name = field.name
        value = form.get(name, default=None)

        if value is None:
            parsed_fields[name] = field.default
            if field.default is None:
                errors[name] = ["Missing"]

            continue

        if field.is_number:
            try:
                value = float(value)
            except ValueError:
                errors[name] = ["Not a number"]
                continue

        parsed_fields[name] = value
        for limit in field.limits:
            error = LIMITS[limit.type](value, limit.value)  # type: ignore
            if error is not None:
                if name not in errors:
                    errors[name] = []

                errors[name].append(error)

    return parsed_fields, errors
