# Uniform - universal backend and frontend code for all your forms.

[![pipeline status](https://gitlab.com/harry.sky.vortex/uniform/badges/master/pipeline.svg)](https://gitlab.com/harry.sky.vortex/uniform/commits/master)
[![coverage report](https://gitlab.com/harry.sky.vortex/uniform/badges/master/coverage.svg)](https://gitlab.com/harry.sky.vortex/uniform/commits/master)